### Connect Via Console/Com Port

1. Open up a terminal window (IE: iTerm)
2. Run `ls -l /dev/tty.*`
3. Connect a USB B Mini Cable to the Port on the Firewall and a Laptop.
4. Back in the Terminal Window, run `ls -l /dev/tty.*` Again
5. Compare the output to the previous output in Step 2. Look for the listing that was not in the output of Step 2 and take note of this as your `Com Device`. It should look like `/dev/tty.usbserial-A10699F5`
6. Ensure `screen` is installed by running `which screen`. Install it if it isn't installed (Not Covered here).
7. Run `screen [Com Device] 38400`. IE: `screen /dev/tty.usbserial-A10699F5 38400`
8. You should now be in the console. Login using `root` and the root password.
	1. If the screen is blank/empty, press any key to get a prompt.

## Reset Admin Password

1. Log into the Firewall via Console/Com Port or SSH
2. Run `cc`
3. Run `RAW`
4. Run `system_password_reset`
5. Navigate to the Web Console. You should immediately see an `Admin Password Setup` Dialog
6. Complete the form and Submit. Take note of your new password!
7. 