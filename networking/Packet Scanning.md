# Live Track/Monitor TCP Dump In Production

## Run on root@ip via ssh/screen

tcpdump -iany -U -s0 -w - 'not port 22' > /tmp/tcpdump

## Run Local

ssh user@ip 'tail -f -c +0 /tmp/tcpdump' | wireshark -k -i -