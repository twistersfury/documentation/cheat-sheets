
## SPX

[GitHub](https://github.com/NoiseByNorthwest/php-spx)

SPX is a profiling utility (similar to xdebug functionality), that has the added benefit of generating a fire chart graph of usage.