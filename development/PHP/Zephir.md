# Gotchas

## Error Code 134

Something has gone horribly wrong...sorta. First, wipe the `ext` folder, as the project is no longer loadable. The most common error of this is due to an invalid Return Type hint or missing `use` Statement. I have also seen it when an overridden method does not have the same return type hint as the parent method.

The easiest way to find it is going to be to comment out sections of the newest code and recompile. After recompiling, recompile it again. If you still get 134, you will have to repeat this process until it goes away. Once it goes away, the most recent comment block has an 'issue' you will have to track down.

*This assumes that your generate and compile commands are running with your extension enabled when the compile commands run.*