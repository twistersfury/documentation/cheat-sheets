## Terminology

- `RAII` - Resource Acquisition is Initialization
	- A standard of using classes where resources are initialized (or passed) in the constructor of the class and then destroyed/reclaimed in the destructor of the class.

## Keywords

- `explicit` - Prevent Auto Conversion Of Parameters on Single Param Constructors

## sprintf Formatters

- `%c` - Single Character
- `%s` - String
- `%d` - Int
- `%o` - Unsigned Int (> 0)
- `%x`  - Hexidecimal
- `%u` - Unicode
- `%f` - Float
- `%lld` - Long Long Int

## Pointers & References

Pointers are one of those fancy things that are really complicated. In simplest terms, a pointer is a variable that stores the memory location of the variable. Think of it somewhat like a map. You pass the 'map' through rather than the actual variable. You must dereference the pointer through the use of `&`.

```C++
MyObject something;
MyObject* somethingElse = &something;

anotherObject.doSomething(somethingElse); // Passing A Pointer
anotherObject.doSomethingElse(something); // Passing Actual Variable
```
## ->

`->` is syntactic sugar for C++. It is the equivalent of dereferencing a pointer and calling a method on the pointed-to object.

```c++
MyObject something;
MyObject* somethingElse = &something;

// These Two Have The Same Result.
(*somethingElse).doSomething();
somethingElse->doSomething();
```

# Errors

## `Cast to 'char *' from smaller integer type 'in_addr_t' (aka 'unsigned int')`

Use Reference (&) on Variable.

```C++
(char *) &varName
```

## function returns address of local variable

You are returning a pointer to a local variable (bad!). It's better to pass in a by-ref and modify it rather than return from the function.

```C++
void Tree::doSomething(Apple &apple) {
    auto fruit = Apple;
    apple = & fruit;
}
```

### Method can be made const

`const` methods do not make any changes to the internal state of the object.

```C++
string Object::getMessage() const {
	return this->message;
}
```

https://discord.com/channels/331718482485837825/1198487434774712321