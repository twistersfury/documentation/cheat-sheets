# OWASP Top 10

*Open Web Application Security Project*

Last updated in 2021, the OWASP Top 10 are the top ten most common vulnerabilities introduced into applications.

1. Broken Access Control - The application fails to enforce permissions or the attacker manages to bypass the access controls.
2. Cryptographic Failures - Not encrypting the data correctly. IE: Using Plain Text or MD5 hashes for storing passwords.
3. Injection - A form of attack that allows the attacker to insert or retrieve information otherwise intended to not be accessible. The most common is SQL Injection (Little Bobby Tables)
4. Insecure Design - A broad category that focuses more on the lack of security implementations at the design level. IE: Anti-Shift Left, Not using SAST Scanning, Etc
5. Security Misconfiguration - Lack or failure to securely configure the application during setup. IE: Failure to disable debug mode in production.
6. Vulnerable and Outdate Components - Not updating 3rd Party Libraries regularly.
7. Identification and Authentication Failures - A broad category that focuses on how well the overall authentication is. While Broken Access Control focuses on the permissions a user has, this category focuses more on how the user is authenticated to the application. IE: Brute Force attacks
8. Software and Data Integrity Failures - Failure to protect against integrity violations like relying on untrusted third-party plugins.
9. Security Logging and Monitoring Failures - Not implementing enough logging and monitoring controls to detect breaches.
10. Server-Side Request Forgery - Making a Server-Side cURL call to a URL supplied by the user request without validating the requested URL.
