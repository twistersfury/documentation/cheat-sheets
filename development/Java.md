# Troubleshooting

### More than one fragment with the name [] was found.

You've checked out multiple versions of the project and the dependencies are left. You need to clean/remove the target folders using something like 

```shell
find . -type d -name "target" -print0 | xargs -0 -I {} rm -rf {}
```