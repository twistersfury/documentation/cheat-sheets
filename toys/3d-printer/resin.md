# Files Not Loading (Photon Mono X 6K)

Likely the file name is too long or has characters the printer doesn't like. Shorten the name and limit it to alpha numerical characters, underscore, or period.
# Wrong Size

If a model is sliced using inches and is imported into CHITUBOX or similar, scale the model by 2540% uniformly to convert it to millimeters.