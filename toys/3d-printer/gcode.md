## Home
G28  

## Auto Level
G29

## Save EPROM
M500

## Load EPROM
M501

## PID Tuning Hot End (From Cold Hot End)

(Turn On Fan To Approximate Run Level)
(Turn On Bed To Approximate Run Temp)
(N = Run Temp, Y = Iterations)

M303 E0 SNNN CY   - `Wait For Completion/Output. Ensure Verbose Mode is Enabled Or You Won’t See It In Simplify3D`

M301 P(P) I(I) D(D)
M500

`M301 P39.11 I5.55 D68.89` 

# Klipper Macros

## ideaMaker Y-Offset

```gcode
SET_GCODE_OFFSET Y=-{layer_height} ; ideaMaker Correction
```

*Note: This will cause errors later in your G-Code, so make sure to run it again at the beginning of the End Gcode With `Y=0`*
## ideaMaker Start Gcode

```gcode
;Nozzle diameter = {machine_nozzle_diameter1}
;Filament type = {filament_name_abbreviation1}
;Filament name = {filament_name1}

SET_ACTIVE_SPOOL ID=4 ; Configure Spoolman

; Generic
PRINT_START TEMP_BED={temperature_heatbed} TEMP_EXT={temperature_extruder1} TEMP_ENC=25

PURGE_LINE BELT_PRINTER={belt_wall_enable} BELT_LAYER_HEIGHT={layer_height} BELT_LAYER_SPEED={belt_wall_speed} BELT_LAYER_WIDTH={belt_wall_flowrate} LAYER_HEIGHT={first_layer_height} LAYER_SPEED={first_layer_speed} LAYER_WIDTH={first_layer_extrusion_width_ratio}

SET_PRESSURE_ADVANCE ADVANCE=0.035
```
## ideaMaker End Gcode

```gcode
PRINT_END
```

##  ideaMaker Layer Change

[Temperature Tower](https://www.thingiverse.com/thing:2729076)

```gcode
TEMP_TOWER CURRENT_LAYER={layer_index} START_TEMP=235 TEMP_STEP=5; Start = Upper Temperature
```