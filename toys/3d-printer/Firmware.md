## Custom Pinouts

It isn't uncommon to need to adjust the pinout. For example, if you have a 3-pin optical sensor and a board that the end-stop port is 2-port, you will have to reclaim another pin for the 3-pin sensor. 

In this case, be aware of what also may be on the board. For example, if there is a resistor between the power pin and the actual board, you may find that the port will not work. IE: You can't use a Neopixel port for an optical sensor.