# Machine Start Gcode
```gcode
PRINT_START TEMP_BED={first_layer_bed_temperature[0]} TEMP_EXT={first_layer_temperature[0]} ENCLOSURE_TEMP={chamber_temperature[0]}

PURGE_LINE LAYER_HEIGHT={first_layer_height} LAYER_SPEED={initial_layer_speed * 60}  LAYER_WIDTH={initial_layer_line_width  * 100} 

; SET_PRESSURE_ADVANCE ADVANCE=0.035
```

