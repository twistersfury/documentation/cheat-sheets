### Layer Height

Maximum layer height should be around 80% of the nozzle diameter, with minimal layer height you should look at the distance the Z Axis moves with one full step of the Z Motor.
### Volumetric Flow  & Max Speed

The fastest you can print is directly proportional to the volumetric flow. To increase the speed at which you can print, you must first determine the volumetric flow.

1. Heat the Hotend to your Target Print Temperature
2. Execute `M83` to enable relative extrusion.
3. Choose a low starting extrusion, IE: 2mm/s.
4. Attach a piece of tape to a ruler at 120mm
	1. The actual mm doesn't matter. Just make sure that the distance is more than 100mm. Additionally, don't worry about exact amounts. IE: If "0" is a few mm away from the edge of the ruler, this is okay. We only want relative measurements, not exact ones.
5. Transfer the tape to the filament by running the filament straight against the ruler edge.
6. Convert `mm/s` to `mm/min` by multiplying the number by 60. IE: `2mm/s = 2 * 60 = 120mm/mm`
7. Execute `G1 E100 F[speed]` where `[speed]` is the mm/min. IE: `G1 E100 F120`. This will extrude 100mm of filament at the given speed.
8. Wait for the extrusion to complete.
9. Transfer the tape back to the ruler.
10. Calculate the difference between the original 'mark' and the new 'mark'. 
11. If exactly 100mm were extruded, repeat steps 3-10 until it starts getting less. This is approximately your max flow rate. You can go beyond this number a bit, but keep faster you go, the more likely you will lose steps. IE: Stay under 2-3% of a dropoff.

Convert your speed from step 11 back to mm/s (divide by 60) and multiply it by `2.4`. This is your max volumetric flow rate.

Once you have your max flow rate, you can calculate your max speed. Your max speed for a given print will be `volumetric flow rate / layer height / layer width`.

So, if you're volumetric flow rate is 6mm/s and you are printing at 100% 0.2 layer height, your max speed is `6 / 1 / 0.2 = 30` or 30mm/s.

If you want to increase your max speed, you will need to print at a higher temperature or a different nozzle/extruder that can handle the higher speed (IE: Using a Volcano Nozzle).

*REF: [Ellis's 3D Print Guide](https://ellis3dp.com/Print-Tuning-Guide/articles/determining_max_volumetric_flow_rate.html)*
### `Silk` Filaments

Silk filaments are resistant to adhesion. To compensate, consider raising the temperature of the bed 10 degrees.

### `Twinkle` Filaments or Filaments with Additives

Filaments with Additives like `Twinkle`, `Sparkle`, or `Carbon Fiber` are typically very abrasive. Additionally, the fibers in the filament can cause a traditional 0.4mm nozzle to jam easily. You should use a 0.5 or 0.6+ nozzle instead to help prevent this.

### `Twinkle` Filaments
Twinkle Filaments are hit or miss on how easy they are to print. It seems it is best to print them at the high end of the filament's suggested temperature range. Further, you may want to slightly increase your extrusion ratio to help prevent jams.