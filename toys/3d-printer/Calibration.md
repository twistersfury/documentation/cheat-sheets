## EPROM vs Firmware

EPROM is the printer's onboard memory. Many printers have this enabled by default in the firmware. It's generally best to store defaults in the firmware rather than via Firmware. This makes it easier to recover from changes (IE: Board Change Out). It's not uncommon to store everything in the EPROM, but know that doing so means that flashing the firmware will require that you 'Restore Defaults' (M503) and Save the EPROM (M500) on every flash. Further, it also means that whenever you do flash the firmware, you will lose any 'tweaks' that weren't part of the firmware. IE: E-Steps. 

I personally think it's better to disable EPROM, or at least always use the Firmware for defaults, especially when first setting the printer up. It'll save you a lot of headaches later if you need to reflash.

## Step Calibration

`Expected Steps / Actual Steps * Current Steps Configuration = New Step Configuration`

- Expected Steps - STL Width/Height/Depth
- Actual Steps - Printed Model  Width/Height/Depth
- Current Steps Configuration
	- Execute `M501`
	- Look For The Line `M92`
		- XNN - X-Axis
		- YNN - Y-Axis
		- ZNN - Z-Axis
		- ENN - Extruder Axis

To Update, Run `M92 [Axis][New Step Configuration]` Followed by `M500`. IE: `M92 Z1146.890`
