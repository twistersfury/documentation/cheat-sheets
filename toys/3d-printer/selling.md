# Thingiverse Attribution Card

Append `/attribution_card` to the thing url to generate a license image automatically.

IE: https://www.thingiverse.com/thing:754095/attribution_card

*Attribute Card Broken: The attribution card is currently broken due to the EOL of the Good Charts API. This can be easily fixed by replacing `chart.apis.google.com` with `quickchart.io`. The below snippet can be used as a Bookmarklet to handle this quickly. It adds jQuery to the page and then quickly replaces the images.*

```js
var newScript = document.createElement("script");newScript.type = "text/javascript";document.head.appendChild(newScript);newScript.src = "https://code.jquery.com/jquery-3.7.1.slim.min.js";setTimeout(function() {jQuery(".qrcode-sidebar img").each(function () {$(this).attr("src", $(this).attr("src").replace("chart.apis.google.com", "quickchart.io"));});}, 1000);
```


# Cost

- 2x Filament Used Cost + $2 / Hour (Saw on Facebook Group)