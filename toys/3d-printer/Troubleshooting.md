# Bad First Layer

If your first layer is bad, chances are that the whole print will fail. At the very least, the bottom layer will look horrible. Most people are aware of the 'Paper' method, where you "level" the bed by using a piece of paper to measure the gap. This method 'works', but it's not the most accurate, because it can be easy to get them off. 

The better method now is to use [Bed Leverler 5000](https://github.com/sandmmakers/BedLeveler5000). This is a Python-based application that you run on your computer to connect to the printer. It does require some pre-configuration to work. Once configured, you then go through the same method of `FL -> BR -> BL -> FR -> CN` of leveling your bed. BL5000 will probe the bed at the point of the bed screw and give you an offset that will look like `2.123`. Your goal is to keep adjusting every screw until all the offsets are the same value, or at least within `0.10` of each other.

*We say we are 'leveling' the bed, but in truth, we are 'tramming' the bed so that it matches the nozzle's movement.*

### BL5000 Steps

1. Set aside a good 20 - 30 minutes. This process takes a while the first time you do it.
3. Adjust all Bed Screws to approximately 3/4 compressed. If you want to go more, you can, but keep in mind you want wiggle room while adjusting.
4. Optional: Use the PrinterInfoWizard tool to create your configuration for your printer. 
5. Connect BL5000. This will auto-home.
6. Heat the Extruder to 170 and the bed to your target bed temperature.
	1. *Technically, your extruder can be the target print temp too, but at 170 it's hot enough to expand, but cool enough to not droop filament.*
7. *Optional:* Probe all points to get an idea of how 'off' everything is.
8. Choose a corner as your `Point of Reference`. 
	1. Typically use the `Front Left` or `FL` as the PoR.
	2. Take note of the offset value. Choose either 0 or a 10th of a decimal as your target. *IE: If your Point of Reference has a value of 3.136, use `3.0` as your target*
9. Adjust Screw
	1. Adjust the screw in the direction of the target.
		1. *Typically Clockwise will make the offset go up, and counterclockwise will make the offset go down.*
	2. Re-Probe the Screw
	3. Repeat until the probe offset reads the target.
10. Repeat Step 9 for each Screw.
	1. Use the diagonal screw to the previous. IE: If you start with FL, use BR next.
		1. *IE: FL -> BR -> BL -> FR*
11. Repeat Step 8 - 10 until all screws show an offset at the same 10th of a decimal.
	1. Expect to do this 5 - 6 rounds the first time.
	2. Keep repeating until you go a full rotation without adjusting any knobs. *IE: If you modify FL, and do not modify BR,BL, or LF, you are good. If the last one you modify is BR, and you do not modify BL, LF, or FL, you are good.*
12. Repeat Step 8 - 10 for 100ths of a decimal
	1. Keep repeating until you go a full rotation without adjusting any knobs.
	2. *IE: If your target was 3.0, and the front left is now 3.054, you now want to target all screws to 3.05*
13. Repeat Step 8 - 10 for 1000ths of a decimal
	1. Keep repeating until you go a full rotation without adjusting any knobs.
	2. *IE: If your target was 3.0, and the front left is now 3.051, you now want to target 3.051*
	3. This is the hardest one to hit. Usually best to try to be at least 0.002 to 0.003 points of the target.

*Note: Due to various factors, probing the same point twice can return different values.*
# Layer Shifting
## One Direction
If the shift is in one direction, it usually means there is something wrong on axis that shifted. Most common is going to be the belt tension isn't tight enough and the belt is skipping on the gear. If that isn't the issue, then chances are it's the motor for that axis is either running under the wrong power settings or worst case going bad. The is also a possibility that your motor drivers are going bad.

If the shift is “diagonal”, then you are running a CoreXY printer and the issues above still apply. Make sure first that the belts are tensioned the same. This is the most common issue. After this, make sure to derack your X-Axis

## Back and Forth

If the shifting seems to be going back and forth, with some layers looking perfect, especially at slower speeds, then your hotend is likely to lose. If you can wiggle the hotend in the direction of the layer shift with your hands, tighten it down. 

# Warping

First try adding a brim of 3+ lines. You can also try increasing the heat of the bed. If using ABS, make sure to use an enclosure.

# Camera Not Working

Likely means that crowsnets has broken due to a recent update of system packages. SSH into the machine and run

```sh
cd ~/crowsnest
make clean
sudo make install
```

and reboot the MCU.