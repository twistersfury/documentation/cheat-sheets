# Actively Monitor Redis Server

ssh redis -L 6379:127.0.0.1:6379

docker run --rm --name redis-commander --env REDIS_HOSTS=host.docker.internal -p 8081:8081 rediscommander/redis-commander:latest