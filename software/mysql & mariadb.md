### Alter Table Using Persona Toolkit

Pt-online-schema-change --dry-run|--execute --host=[db] --user=[user] --ask-pass --alter=“ADD COLUMN doSomething INT(11) UNSIGNED DEFAULT 0 NULL” D=schema,t=[tableName]

## Useful Queries
### Query - List Processes

```sql
select * from INFORMATION_SCHEMA.PROCESSLIST where db = schema order by TIME_MS DESC;
```

### Query - Build Kills For Sleeping Processes

select CONCAT ("KILL ", ID, "") from INFORMATION_SCHEMA.PROCESSLIST WHERE COMMAND = "Sleep" AND TIME > 60 AND USER IN (‘user1’, ‘user2’) order by TIME_MS DESC;

### Query - List Hosts Connected

select SUBSTRING_INDEX(HOST, ':', 1), COUNT(HOST) from INFORMATION_SCHEMA.PROCESSLIST where db = 'db' GROUP BY SUBSTRING_INDEX(HOST, ':', 1) order by TIME_MS DESC;

### Query - Database Total Size

SELECT table_schema AS "Database”, ROUND(SUM(data_length + index_length) / 1024 / 1024, 2) AS "Size (MB)"  FROM information_schema.TABLES  GROUP BY table_schema;

### Query - Database Table Sizes

SELECT table_schema AS 'Database', table_name AS 'Table', ROUND(SUM(data_length + index_length) / 1024 / 1024, 2) AS "Size (MB)" FROM information_schema.TABLES GROUP BY table_schema, table_name ORDER BY table_schema, data_length + index_length DESC;

### Query - Database Table Sizes - Specific Table
```sql
SET @tableName='';
SELECT table_schema AS 'Database', table_name AS 'Table', ROUND(SUM(data_length + index_length) / 1024 / 1024, 2) AS "Size (MB)" FROM information_schema.TABLES WHERE TABLE_NAME LIKE @tableName GROUP BY table_schema, table_name ORDER BY table_schema, data_length + index_length DESC;
```

### Process List
```sql
select * from INFORMATION_SCHEMA.PROCESSLIST where db = 'db' ORDER BY TIME_MS DESC;
```

### Kill Pending Connections 
```sql
select CONCAT ("KILL ", ID, "") from INFORMATION_SCHEMA.PROCESSLIST WHERE COMMAND = "Sleep" AND TIME > 30 AND USER IN (‘user’, ‘user2’) order by TIME_MS DESC;
```