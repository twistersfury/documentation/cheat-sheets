Useful Azure Log Queries

### Rewriting URLs to Exclude IDs

```kql
Nginx_CL
| where Uri_CF startswith "/something"
| extend Uri = replace(@"/(.*?)(\d+)/(.*?)", @"\1/\3", Uri_CF)
| extend Uri = replace(@"/(.*?)(\d+)", @"\1", Uri)
| extend Uri = replace(@"/(.*?)/-(\d+)--(\d+)-(\d+)", @"\1", Uri)
| extend Uri = replace("//", "/", Uri)
| extend Uri = replace(@"^/(.*)", @"\1", Uri)
| extend Uri = replace(@"(.*)/$", @"\1", Uri)
| summarize by Uri
```

```kql
Nginx_CL
| where Uri_CF startswith "/something "
| extend Uri = replace(@"/(.*?)/(\d+)/(.*?)", @"\1/[param]/\3", Uri_CF)
| extend Uri = replace(@"/(.*?)/(\d+)/(.*?)", @"\1/[param]/\3", Uri)
| extend Uri = replace(@"/(.*?)/(\d+)/(.*?)", @"\1/[param]/\3", Uri)
| extend Uri = replace(@"/(.*?)/(\d+)/(.*?)", @"\1/[param]/\3", Uri)
| extend Uri = replace(@"/(.*?)/(\d+)/(.*?)", @"\1/[param]/\3", Uri)
| extend Uri = replace(@"/(.*?)/(\d+)$", @"\1/[param]", Uri)
| summarize count() by Uri
```

### CPU Usage By Machine

```kql
Perf 
| where CounterName == "% Processor Time" and InstanceName == "_Total" 
| summarize AggregatedValue = percentile(CounterValue, 90) by bin(TimeGenerated, 1m), Computer
| render timechart 
```

### Response Time Percentile

```kql
Nginx_CL 
| summarize AggregatedValue = percentile(ResponseTime_CF, 90) by bin(TimeGenerated, 5m)
| render timechart
```

