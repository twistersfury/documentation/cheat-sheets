### Listing Open Ports

```bash
netstat -l # All Ports
netstat -lt # TCP Ports
netstat -lu # UDP Ports
```

### Testing TCP Ports

```bash
telnet host.com 1234
```

### Testing UDP Ports

```bash
nc -z -v -u host.com 1234
```