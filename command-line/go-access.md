https://goaccess.io

## Stream Nginx Logs Locally

ssh user@server 'tail -f /var/log/nginx/access.log' | goaccess --date-format=%F --time-format=%T --log-format='server="%v" %^ %^ %^ %^ %^ time_local="%dT%T" protocol="%H" status="%s" bytes_out="%b" %^ http_referer="%R" http_user_agent="%u" %^ http_x_forwarded_for="~h{, }" %^ uri_query="%q" uri_path="%U" http_method="%m" %^ %^ request_time="%T"'