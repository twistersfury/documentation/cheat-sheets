## Command Line Usage

`jmeter -n -t test.jmx -l results.jtl -j results.log -p dev-server.properties`

- `-n` - Test Mode
- `-t ...`  - The Test File
- `-l ...` - The results JTL File
- `-j ...` - The general process log file.
- `-p ...` - The property file to use (See )