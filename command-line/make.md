# GNU Make

`make` is most commonly used in compiling software. Most Linux machines come with it already installed or can be easily installed via the package manager. While used for compiling software, it also serves as a way to create project command aliases that live with the project.

## CMake != Make

You may see references to `CMake` when researching it. Not that `CMake` and `Make` are two separate software, though they are related. Specifically, `CMake` is commonly used in C++ projects. `CMake` auto-generates the `Makefile` for a given project, based on the configuration contained. `CMake` is focused on the `Metadata` of a given project, while `Make` is focused on Building the project.

# Make Crash Course

## The Basics
On the surface, make works by targeting a file named `Makefile` (no extension). This file will typically live in the project's base directory but may be deeper depending on sub-modules. You may occasionally see the `.mk` extension on non-traditionally named files.

This file specifies a series of `target`s and their dependencies in order to work. When a given `target` is triggered, make will check all the dependencies, in order, to see if any have changed since the last execution. If they have, `make` will proceed to run that dependency and all subsequent dependencies before running the actual target. It's worth noting that all dependencies of dependencies will be checked as well.

# Pseudo Code Makefile Sample

```Makefile
hungry:
	Yep, I'm hungry

goto-store:
	Going to the Store

bread-loaf: goto-store
	
bread: bread-loaf
	Cut Slice of Bread

ham: goto-store

cheese-slice: goto-store

sandwich: bread-slice bread-slice ham cheese-slice | hungry
	Get 2 Slices of Bread
	Put Ham on One Slice of Bread
	Put Cheese on One Slice of Bread
	Put Slices Together
```

## Target Syntax

### Normal File

```Makefile
[target]: [dependencyN]
	[command1]
	[command2]
	[commandN]
	
file: dependency-1 dependency-2
	echo "file" > $@
```

`make`'s normal purpose is to create files. The `target` is the file name. You can have zero or more `dependencyN`. These are targets that must exist in order for the `target` to be created.  Make will recursively check each dependency to see if either A) The file doesn't exist or B) The file's modification date is newer than the `target`. If either of these conditions is met, make will rerun the current `target`. When `make` runs on any given target, it will execute all the `command`s in order until one fails. The target is considered successful if all the commands are completed without error.
### Check Dependency Exists

```Makefile
bin/:

bin/file.txt: | bin/
	echo "file" > $@

```

You may also have dependencies that just need to exist, meaning that `make` will ignore the modification check on only check if the location exists. This is done using the `|` symbol on the target dependency list. Everything before the `|` will have a modification check, everything after will not. This is most commonly used for folders, as the default folder modification date also changes with the files. This means that any file will trigger a full rebuild if the folder is registered as a change.

### Fake Targets

```Makefile
.PHONY: start
start:
	Do Something
```

You may occasionally want targets that don't generate files, or you want the target to always run even if the files it generates already exist. These targets must be listed in the file with the `.PHONY` keyword. Otherwise, you get an error to the effect of `Nothing to be Done`. 

## Environment Variables

You are able to use any number of environment variables in the `Makefile`. These will commonly be used for either configuration or ease of maintenance. Environment variables will be prefixed with `$` when being referenced.
### Reserved Variables

- `$@` : This is used to represent 'the current target'. If your target is `bin/file.txt`, then `$@` means `bin/file.txt`. In this way, you don't need to constantly rewrite your target.

### Defining Variables

Variables are defined using a few operators. The operator determines how the variable will react when it is redefined. The definition always follows the format `VARIABLE_NAME [operator] value`. Variables should be named in `ALL CAPS`. Keep in mind that everything in the variable will be used, including quotes, when specified in the Makefile. The variable 'ends' at the newline character. 

#### `=` Always Assign Operator

The `=` operator is the simplest. It will always set the variable, even if already defined.

```Makefile
MY_VARIABLE=some value
MY_VARIABLE=other value

// MY_VARIABLE == other value
```

#### `:=` Assign Once Operator

The `:=`  operator will assign the operator in a manner that it can't change once assigned. 

```Makefile
MY_VARIABLE := some value
MY_VARIABLE := other value

// MY_VARIABLE == some value
```

#### `?=` Assign If Not Assigned Operator

The `?=` will only set the variable if the variable has not yet been defined. This is basically a 'default' style setup, where the value is used only if not set otherwise. This will commonly be used for in-line commands and/or file overrides.

```Makefile
MY_VARIABLE ?=some value
MY_VARIABLE ?=other value

// `make MY_VARIABLE='command value'`, MY_VARIABLE == command value
// `make`, MY_VARIABLE == some value
```