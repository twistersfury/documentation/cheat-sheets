Useful Git Commands

- Search For Words 
	- `git log -S Words`
- Search For File (Even Deleted) 
	- `git log --all --full-history -- <path-to-file>`
	- `git log --all --full-history -- "**/file-name.*"`
- Show Commit Details 
	- `git show <Hash>`
- Branches That Have Been Merged Into Master 
	- `git branch --merged master`
- Delete Branches Merged Into Master/Upstream
	- `git branch --merged upstream/master | xargs git branch -D`
- Delete Branches From Upstream
	- `git push --delete upstream branchname`
- Change Upstream Tracked Branch
	- `git branch branch_name --set-upstream-to your_new_remote/branch_name`
## Introducing Git Attributes File

```sh
git add .gitattributes
git commit -m "Adding .gitattributes"
git rm --cached -r .
git add -A
git commit -m "Resetting Attributes"
```
## Git Attributes Changes Files - Reports "Contents Identical" in JetBrains

```sh
git rm .gitattributes
git add -A
git reset --hard
```


## List All Tags Not Merged Into the Current Branch
```sh
git tag --no-merge
```

# Rename Branch
```sh
git branch -m oldname newname
git branch -a
```

# Git Bash on Windows w/Linux Paths

When running any commands on Git Bash that includes Linux style paths, you may get errors that mention the path to git bash. IE: `This name is not in that format: 'C:/Program Files/Git/C=XX` When running an Open SSL Command that has `-subj "/C=XX"`. This is because Git Bash is trying to be smart about the path. In order to fix this, you should execute

```bash
export MSYS_NO_PATHCONV=1
```

Adding this to your `~/.bash_profile` file in Git Bash will prevent this error by forcing git to not auto-convert `/` into the Program Files path.
