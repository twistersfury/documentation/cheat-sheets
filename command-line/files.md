### Rename All Files In Folder (IE Replacing Space) 
```bash
for file in *; do mv "$file" `echo $file | tr ' ' '_'` ; done
```

### List Total File Sizes Using LS
```bash
ls -FAGl "${@}" | awk '{ total += $4; print }; END { print total }';`
```

### List All Files Sorted By Size Using Find/Sort

```bash
find . -type f -print0 | xargs -0 ls -al | awk '{print $5 " " $9$10$11$12$13$14$15$16}' | sort -k 1 --numeric
```

### List All Files Sorted By Size Using Find/Sort (Leaving `ls` Information)
```bash
find . -type f -print0 | xargs -0 ls -al | sort -k 5 --numeric
```

### List Processes On Open File

```bash
lsof /path/to/file
```

### Filter Files By Checksum (FreeBSD)

```shell
$ cat checksum.md5 | sort | cut -d ' ' -f 1 | uniq -d | xargs -I {} grep {} checksum.md5
```