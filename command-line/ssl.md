## Downloading Cert For Site

```shell
openssl s_client --connect host:443 -showcerts </dev/null | openssl x509 -outform pem > cert.crt
```