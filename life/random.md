## Celsius to Fahrenheit

#### Exact

```
Fahrenheit = (Celsius * 9/5) + 32
```

#### Rough

```
Fahrenheit = Celsius * 2 + 30 +- 3
```

#### Matrix

| Celsius | Fahrenehit | Comment |
| ------- | ---------- | ------- |
| 0 | 32 | Freezing
| 10 | 50 |
| 15 | 59 |
| 20 | 68 |
| 25 | 77 |
| 30 | 86 |
| 40 | 104 |
| 100 | 212 | Boiling Water
| 200 | 392 | PLA Filament |
| 230 | 446 | ABS Filament |