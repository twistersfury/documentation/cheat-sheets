
## Workout Target

150 minutes a week moderate (300 better) or 75 vigorous 

## Calculations
220 - (Age) = (Max Heart Rate)
(Max Heart Rate - Resting Heart Rate) = (Heart Rate Reserve)
((Heart Rate Reserve) * 0.7) + (Resting Rate) = Moderate Workout Minimum

Multiply by 0.85 for moderate cap/vigorous workout