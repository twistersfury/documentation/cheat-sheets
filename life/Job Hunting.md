Last Modified 2023-06-18. Prices and information may have changed. These are my personal opinions. You may follow them at your own discretion and risk.

## Background

On December 7th, 2023, my company announced a big layoff. My first thought was "Oh no, who are we losing." Little did I know or expect that I would be the one the team lost. This came as a complete surprise to not only myself, but my team as well. I had been working for the product for over 8 years, and was the most knowledgeable about the product, with all but 1 person having been there less than a year. While I understand a lot of people were laid off during this time, this caught my team and me by surprise. Most of the people I talked with all felt that there had to be politics going on, and agreed that I shouldn't have been let go. I will likely never know the truth, but suffice it to say I wasn't prepared. I consider myself blessed that I wasn't let go immediately, but I still had to dive back into the job market, something I wasn't ready to do.

I had to immediately learn a lot about the new job market and how best to get a new job. I was competing with 300,000+ other people. Additionally, I was seeing rumors of people who had been unable to find a new job and had been actively searching for over 6 months. I couldn't go 6 months without a job, so I did a lot of research to help. I've put a summary of that information here. I hope it helps others.

## Terms

### Applicant Tracking System (ATS)

Any companies that allow online applications likely have an ATS. The ATS will scan your resume and rank your resume against the job posting. You must tailor your resume as much as possible to each job posting. The ATS will then weigh your resume against all other resumes that have been submitted. When recruiters are reviewing applicants, they will look at the resumes that have the highest weight/match to the job posting. 

Some statistics state that only 3% of applications are even reviewed as a result of the ATS. This means that if your application isn't in that top 3%, your application won't even be opened.

It is worth noting that in the USA, there are laws that require all applications to be "reviewed". As a result, many recruiters will deny the above statement regarding 3%, claiming that they always look at every application, regardless of what the ATS says. While I'm sure this is true, I suspect they are fudging the details a bit. For example, on LinkedIn, the submitted application review screen shows a minimum amount of information. This is the "application", and this information will say if the candidate has the key skills or not. They can see this screen without ever touching your resume. This means they can "technically" look at every application, but never see more than 10% of the information. Additionally, I believe that when the ATS ranks the applicants, they will only set up so many top applications. If during that process the hiring manager likes a specific person, they will hire that specific person without looking at any more applications, because the role has been filled.

### Tailored Resume

You should tailor your resume to every job application you put in, especially if you either consider yourself to be a top applicant, or it is one of those jobs you "must have." Tailoring your resume is a tedious process, but means adjusting your resume to do a few things for each application:

1. Keyword Cloud - Run the job application through a keyword utility, and implement the top-used words into your resume.
2. Responsibilities Match - Most job postings will list the expected responsibilities. You should add bullet points to directly match these responsibilities. For example, if the responsibility is `Track Monthly Team Budget` and this is something you have done, then at the position on your resume that you did this, you should have something similar to `Tracked monthly team budget, lowering costs by 10%`
3. Title Match - In certain job markets, it's not uncommon to see the same role being defined under a different title. `Software Engineer` and `Developer` are more or less interchangeable. If your resume says `Java Software Engineer`, but the job posting says `Java Developer`, then you should adjust your resume to say `Java Developer` instead. This will match Keywords better as well.
	1. **A Word Of Caution**: While I believe most hiring managers will not care about formal titles in this scenario, the one spot that it will matter is the background check. Depending on how in-depth the background check is, doing this could complicate the process. 

## Useful Sites & People

### Cultivated Culture

#### About

https://cultivatedculture.com

Cultivated Culture has several useful tools to use while job hunting.

1. Resume Rater - Upload your resume and have it 'ranked'.
2. E-Mail Searcher - If you have identified a contact in the company to send your resume to, enter their name and the company domain, and it will attempt to tell you what their e-mail address is.
3. Bullet Rater - Paste in a bullet point from your resume, and it'll rank how good the bullet point is.
4. Articles - Plenty of articles to help on a job hunt.

#### Cost

The free edition gets you a limited number of tokens per day. Each tool costs a single token.

The paid edition is $9.99 / Month. This gives you unlimited tokens so you can run the tools as much as you want.

### Resume Worded

#### About

https://resumeworded.com

Resume Worded uses a formal ATS and AI backend to review your resume and ultimately rank it. This is similar to what Cultivated Culture does but is far more in-depth and advanced than the one in Cultivated Culture. It includes two separate modes. The first allows you to improve the overall quality of the resume, while the second will compare it to a job posting and rank it directly against the job posting.

#### Cost

RW is a bit more costly, coming in at over $50 / Per Month.

### LinkedIn

https://www.linkedin.com/

Most people know about linked in. It is effectively the social media platform for businesses. What many people may not realize is that it also serves as your "online resume". Many recruiters will actively recruit through LinkedIn. Further, many hiring managers will look at your linked in profile in addition to looking at your resume. With this in mind, you should make sure to update your LinkedIn profile to put your best foot forward. There are plenty of YouTube videos on how to do this, so I will only say the following:

1. Update your tagline. Be sure to include the keywords that recruiters will search for.
2. Update your `About` to show your personal side.
3. Update your job history.
4. Update your skills.
5. NETWORK!! NETWORK!! NETWORK!! Friend as many people as you can. 
6. POST!! LIKE!! COMMENT!! The more active you are, the more likely your profile is to be seen by recruiters.
7. Ask for `Recommendations` from anyone you are close to.
8. Ask for `Endorsements` from any of your co-workers.
9. Consider Purchasing LinkedIn Premium. 
	1. LinkedIn Premium gets you a few extra features. The most useful in my opinion is access to the exclusive group. You also get the ability to see up to 90 views of your profile, in addition to using the InMail feature to contact recruiters without being in their network.

#### Cost

LinkedIn is free. LinkedIn Premium is $49 Per Month.

### Amir Satvat (LinkedIn)

https://www.linkedin.com/in/amirsatvat/

In light of all the layoffs lately, Amir has been a God Send. The work he's done with his `Games Job Directory` merits an immediate mention in my mind. His 9.0 version is linked here: https://www.linkedin.com/posts/amirsatvat_diabloiv-activity-7073896069654310912-V0WC?utm_source=share&utm_medium=member_desktop


### Shanda Hancock

https://www.linkedin.com/in/shandahancock/

Shanda is a recruiter for EA Games. I mention her because she too was posting videos on how best to look for jobs in the gaming industry. As she was willing to talk to me directly about my questions, I also wanted to mention her.

## Tips & Tricks

### Networking
Sadly, the old adage of `It's not what you know, it's who you know.` Really tends to apply when job hunting. As a result, networking is something you should do as much as possible. This is especially true with LinkedIn. The more people in your network, the more people that will see your posts. They can then share your posts with their network, getting you even more visibility.

### Avoid LinkedIn Easy Apply
LinkedIn's Easy Apply is an amazing feature. It allows you to quickly apply to many different jobs very quickly. However, this means so can everyone else. If the job uses Easy Apply, chances are you will see the numbers fly up quickly, resulting in less of a chance of them seeing you.

With that in mind, it's worth noting that in some instances, Easy Apply will just send the application to the company's AST. In this case, use your discretion. You can try to find the formal AST (By looking for the `Careers` or `Jobs` page on their website), or use Easy Apply. I tended to use the AST.

### Like, Post, and Comment

When talking specifically about LinkedIn, be as active as possible. You even want to be active when not searching for a position. This will improve the chances of your profile showing up when recruiters search for potential hires.

### Watch Out for Resume Farms

For various reasons, I'm not going to directly mention any resume farms, but make sure you research any companies you apply to before applying. If you don't know the literal company, you want to make sure it's legit. A "Resume Farm" is a company that will take every resume they can get, then "rapid fire" those same resumes to any job application they can. In many cases, they may not even make sure it's a full match.

Try to get as many details about the job position as you can, including the name of the company. If the recruiting company isn't willing to give it, then it may be worth reconsidering.