Useful Server Commands

- Scan SMTP Log
	- `ssh <server> 'cat /var/log/maillog* | /root/pflogsumm-1.1.1/pflogsumm.pl' | less`
- SSH w/Socks Proxy
	- `ssh -D 10000 <user>@<ip>`
- SSH w/Debug (Requires Update of XDebug Port In PHP Storm)
	- `ssh <user>@<ip> -R 9000:127.0.0.1:9999`
- Compare Server Node File Contents To Another
	- Everything
		- `ssh server-1 "find /path/to/check -type f | tr '\n' '\0' | xargs -0 md5sum" | grep -v storage\/ | grep -v \.volt | grep -v checksum.md5 | grep -v \.env | ssh server-2 '(cd /path/to/check && md5sum -c)' | grep -v OK`
	- Existing Files Only
		- `ssh server-1 "find /path/to/check -type f | tr '\n' '\0' | xargs -0 md5sum" | grep -v storage\/ | grep -v \.volt | grep -v checksum.md5 | grep -v \.env | ssh server-2 '(cd /path/to/check && md5sum --ignore-missing -c)' | grep -v OK`