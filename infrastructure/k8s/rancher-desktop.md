## Shell Into RD Lima “Host”
	- `rdctl shell`

## Fix Clock Time

On Windows, RD uses `WCL`. WCL has a bug that if the computer goes to sleep, the WCL 'clock' stops tracking. Once the computer is back up, if you create a new pod, the pod will show an arbitrary number for the age (IE: 12 Hours), which is the total cumulative time the computer has been sleeping. This doesn't inherently harm anything but it can be annoying. In order to fix this, you can restart your computer. Alternatively, execute the command below in the terminal to reset it.

*Note: Resetting the clock only works for pods/services moving forward. It doesn't correct the already created pods.*

```shell
rdctl shell date
rdctl shell sudo hwclock -s
rdctl shell date
```

## Docker Insecure Registries

```shell
rdctl shell sudo vi /etc/conf.d/docker
```

Edit `DOCKER_OPTS`

```
DOCKER_OPTS="--insecure-registry=insecure.home:80 --insecure-registry=insecure.home:443"
```

and Restart Rancher Desktop. If you still get errors, then:

```shell
openssl s_client -connect insecure.home:443
```

Copy the cert (`-----BEGIN CERTIFICATE-----` to `-----END CERTIFICATE-----`) and then

```shell
mkdir -p ~/.docker/certs.d/insecure.home
vi ~/.docker/certs.d/insecure.home/docker_registry.crt
```

and paste the cert and restart RD. 

*Note: This assumes that lima is symlinking `/etc/docker/certs.d` to `~/.docker/certs.d`