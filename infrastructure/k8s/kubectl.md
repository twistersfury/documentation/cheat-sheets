# Kubernetes (K8S)

`K8S` is a container orchestration platform. You tell K8S that you need 3 pods for a specific image, and K8S will make sure that condition is always met. The primary command for K8S is the `kubectl` command.

## K8S Crash Course

### API Objects
All services/objects in K8S are API Objects. These are configured into the heart of K8S, though additional ones can be added. The most common API Objects are defined below.

#### Node

This is the server machine/VM. Every machine/VM in the 'cluster' is a node.

#### Namespace

All your services for a given deployment will typically be kept in/under a specific namespace. You can have them under different namespaces, but it is more complex to set up communication between namespaces.
#### Container

Don't get caught up on Containers. In Docker or Docker Compose, this is the 'base unit'. This is not the case in K8S. Ultimately, the container is the running docker image and is otherwise no different from a docker container running in traditional docker.
#### Pod

This is considered the `base unit` of K8S. A pod is composed of one or more `Containers`. Generally speaking, there will be one container running per pod. In the event there is more than one, the first 'container' listed in the configuration is the main container, and all other containers are considered `sidecars`.

##### Pods As a Single Unit

This is technically an advanced subject, but can really bite you if you aren't careful. In Docker/Docker Compose, a container is completely isolated. In a Pod, all containers in the pod exist as if part of the same unit. A couple of the results of this are:

- All containers must exist on the same node. Otherwise, 
- If one container shuts down, the pod is restarted.
- The `localhost` is shared on a pod. You cannot have two containers listening on the same port in a pod.

#### Deployment/Stateful Set

In order for a pod to be maintained, it has to be behind either a Deployment or a Stateful Set. These objects are responsible for ensuring the pod always exists in the create state. A `Deployment` focuses on transient or stateless pods, while a `Stateful Set` focuses on stateful pods.

#### Services

As pods are intended to be ephemeral (Short-lived), the IPs on these pods can change at any given time. This can make it difficult to keep proper connections to pods. A service acts as a mini-load balancer by routing traffic to any of the running pod instances.

#### Ingress

The ingress is responsible for routing outside traffic into the cluster/pod configuration.

#### Persistent Volume Claim/Persistent Volume

When sharing data among pods or for stateful data, PVCs are most commonly used. These are backed by a PV. Typically speaking, DevOps will own the PV, while the team will own the PVC.

# `kubectl` Crash Course

# Basic Format

``` shell
kubectl [action] [api object] [options]

kubectl get pod
kubectl get svc
kubectl get all
kubectl create secret
kubectl delete pod [pod name]
kubectl logs [pod name]
kubectl logs -c container-name [pod name]
```

# Gotchas

#### Stateful Set Domain Resolution

Stateful sets must use the `serviceName` to access the raw version. Normally, you use `pod-name.namespace.svr.cluster.local` to access the pod. For StatefulSets, you must instead use `pod-name.serviceName.namespace.svc.cluster.local`

#### Node Port & Readiness Probes (Debugging Boot)

Due to the way a readiness probe works, this will block all attempts to access node ports until the readiness probe succeeds. This means that you will be unable to connect to anything on boot unless you disable the readiness probe. For example, debugging a Java command on initial boot.

# Advanced Commands

### Create GitLab Secret For Pulling Images
```bash
kubectl create secret docker-registry registry-credentials --docker-server=registry.gitlab.com --docker-username=<User Name> --docker-password=<Access Tokan>  --docker-email=<account e-mail>
```

### Copy Files From Local To Remote Image
```bash
kubectl cp /tmp/foo <some-namespace>/<some-pod>:/tmp/bar
```

### Restart Core DNS (DNS Not Resolving In Pods)

```bash
kubectl -n kube-system rollout restart deployment coredns
```

### Scale Pods

```shell
kubectl scale --replicas=2 deployment deployment-name
```

### Filtering Pods For Use In Terminal

```sh
kubectl get pods -l tf.deployment=php --no-headers -o custom-columns=":metadata.name"
```

- `-l tf.deployment=php` - The label to filter by and the value to filter by.

### Restarting All Pods In A Deployment

```sh
kubectl rollout restart [deployment]
```

#### Restarting Specify Deployment Via Label

```sh
kubectl get deployment -l tf.deployment=php --no-headers -o custom-columns=":metadata.name" | xargs -I {} kubectl rollout restart deployment {}
```
### Debugging Pods

`kubectl debug` Will set up an ephemeral container that runs alongside your actual container. This can be used to debug the pod using other images and allows you to access the file system.

```bash
POD_NAME=$(kubectl get pods -l tf.deployment=php -o jsonpath='{.items[0].metadata.name}')
kubectl debug -it -c debugger --target=php --image=busybox ${POD_NAME}
```

### Accessing Files

When using `--target` or `--share-processes`, the target pods filesystem can be found under `/proc/[id]/root`, where `[id]` is the process id (typically `1`).

## K9S

https://k9scli.io

Not technically Kubectl, but really good GUI for kubectl (Terminal Based, so no mouse).

You use the keyboard for all interactions. Use `:` to swap between api objects. Pressing `enter` will select or 'go into' the given line item. For everything else, you can reference the key legend at the top of the GUI.