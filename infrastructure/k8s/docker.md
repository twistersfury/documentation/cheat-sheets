## Build Image On Different Machine / Host

```sh
DOCKER_HOST="ssh://user@host" docker build ...
```

*Note: This will build the image on the remote machine, not your local machine, by default.*

## Download Image From A Different Build Host

```sh
DOCKER_HOST="ssh://user@host" docker image save img | docker image load
```

## Copy Image From One Repo To Another Repo

```shell
docker run --rm regclient/regctl image copy registry.gitlab.com/twistersfury/docker-images/phalcon:81-1.0.36-unstable-arm-builds.29-development twistersfury/phalcon:81-development --host reg=registry.gitlab.com,user=[username],pass=[password] --host reg=docker.io,user=[username],pass=[password]
docker.io/twistersfury/phalcon:81-development
```

This trick runs the `regctl` client, which works directly with the API. Instead of pulling the image locally and then pushing it to the remote, it does a straight API to API copy of the image. Replace `[username]` and `[password]` with the relative registry credentials.

## Docker Mounts on Windows (Rancher Desktop)

The traditional `-v $(pwd):/path` does not work on Windows w/Rancher Desktop. Instead, use `-v /mnt($pwd):/path`. This will correctly handle the local folder being mounted to `/mnt/c` in the VM. Alternatively, running `ln -s /mnt/c /c` may work as well.

## Query GitLab Runner For Running Processes 
	
`docker-machine ls | grep kycsugks | grep Running | cut -d ' ' -f 1 | xargs -I {} docker-machine ssh {} 'hostname && docker ps'

## Reset Docker Date/Time (When Not In Sync With Windows)

	`docker run --rm --privileged alpine hwclock -s`

## Prune Commands (Cleanup)
	- `docker system prune`
	- `docker builder prune -af`
	- `docker buildx prune`

## Simple Server Deployment

```shell
docker run --rm -it -v "$(pwd)":/usr/share/nginx/html -p 8080:80 nginx
```

Use `/mnt$(pwd)` when running on windows.