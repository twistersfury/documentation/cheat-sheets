# Overview

Helm is a templating engine for K8S. It allows you to easily configure K8S deployments. Without it, you would have to keep multiple copies of the K8S configuration files for different environments. The Helm documentation is extremely detailed, so there will mostly be advanced stuff here.

## Helm Crash Course

### Commands

- `helm repo [command]` - Commands pertaining to repos
- `helm upgrade --install [command]` - Install a chart if not installed, update otherwise.
- `helm uninstall [command]` - Uninstall chart
- `helm template [command]` - Output raw K8S files.

### Values

One of the greatest advantages of helm is the 'values', typically called `values.yaml`. Anything defined as a value can be leveraged in the Templates. You can also specify multiple helm files based on the commands.

### Templates

It's worth noting that Helm `Templates` are just normal K8S configuration files, with variables and conditions. When you run `helm upgrade`, it parses the templates into the fully qualified/parsed K8S configuration and then automatically runs `kubectl apply` for said templates. For the most part, when writing helm templates, just write valid K8S configuration files. Then add in variables from the values, or the advanced template config.