## Output Sensitive Information

output.tf

```terraform
output "password" {
  sensitive = true
  value = random_password.password.result
}
```

```sh
terraform apply
terraform output -raw password
```